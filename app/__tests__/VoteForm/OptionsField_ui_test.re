[@bs.config {jsx: 3}];

open Jest;
open Expect;
open JestDom;
open ReactTestingLibrary;


/**TODO: test click event with bs-webapi lib https://testing-library.com/docs/bs-react-testing-library/examples */

describe("OptionsField UI", () => {
  
  let element = (el) => (
    ReasonReact.cloneElement(
      el,
      ~props={"data-testid": "options-id"},
      [||]
    )
  );

  let monoVoteOptions: array(VoteTypes.Option.t) = [|
    {name: "Yay", value: 1},
    {name: "Nay", value: 2}, 
    {name: "Pass", value: 3}
  |];

  let threeButtons = element(<OptionsField options={monoVoteOptions} doVote={ignore} choice={1}/> ) 
    |> render 

  test("OptionsField has first button when multiple three are passed", ()=> 
    threeButtons
    |> getByText(~matcher=`Str("Yay") )
    |> expect
    |> toBeInTheDocument
  )
  test("OptionsField has second button when multiple three are passed", ()=> 
    threeButtons
    |> getByText(~matcher=`Str("Nay") )
    |> expect
    |> toBeInTheDocument
  )
  test("OptionsField has third button when multiple three are passed", ()=> 
    threeButtons
    |> getByText(~matcher=`Str("Pass") )
    |> expect
    |> toBeInTheDocument
  )

  test("OptionsField button has class 'selected' when the option matches choice", ()=> 
    threeButtons
    |> getByText(~matcher=`Str("Yay") )
    |> expect
    |> toHaveClass(OptionsField.Styles.selected)
  )
  
  test("OptionsField button does not have class 'selected' when the option does not match choice", ()=> 
    threeButtons
    |> getByText(~matcher=`Str("Nay") )
    |> expect
    |> Expect.not_
    |> toHaveClass(OptionsField.Styles.selected)
  )

})