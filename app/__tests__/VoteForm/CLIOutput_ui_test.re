[@bs.config {jsx: 3}];

open Jest;
open Expect;
open JestDom;
open ReactTestingLibrary;


/**TODO: test click event with bs-webapi lib https://testing-library.com/docs/bs-react-testing-library/examples */

describe("CLIOutput UI", () => {
  
  let element = (el) => (
    ReasonReact.cloneElement(
      el,
      ~props={"data-testid": "options-id"},
      [||]
    )
  );

  //TODO: can't test show or choice=0 props because I can't get the textarea element without text populating it :/

  test("CLIOutput interpolates props in output correctly", () => 
    element(<CLIOutput addr="MyCoolAddr" voter="VoterAddr" choice={3} show={true} />)
    |> render
    |> getByText(~matcher=`Str("tezos-client transfer 0 from VoterAddr to MyCoolAddr --arg '3' --entrypoint \"vote\""))
    |> expect
    |> toBeInTheDocument
  )
})