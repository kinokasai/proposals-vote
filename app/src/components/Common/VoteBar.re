[@bs.config {jsx: 3}];

module Styles = {
  open Css;

  let barLabel =
    style([
      width(`percent(25.)),
      textAlign(`right),
      paddingRight(`rem(1.)),
    ]);

  let barContainer =
    style([
      height(`rem(0.75)),
      width(`percent(90.0)),
      padding(`px(0)),
      backgroundColor(`rgb((240, 245, 245))),
      borderRadius(`px(5)),
    ]);

  let bar = style([height(`percent(100.)), borderRadius(`px(5))]);
};

[@react.component]
let make = (~percent, ~name, ~bar_color="green") => {
  let percent_str = percent->BigNumber.to_string ++ "%";

  <tr className="voting-bar">
    <td className=Styles.barLabel> name->ReasonReact.string </td>
    <td>
      <Tooltip tooltip_text=percent_str>
        <div className=Styles.barContainer>
          <div
            className=Styles.bar
            title={"Value: " ++ percent_str}
            style={ReactDOMRe.Style.make(
              ~width=percent_str,
              ~backgroundColor=bar_color,
              (),
            )}
          />
        </div>
      </Tooltip>
    </td>
  </tr>;
};