[@bs.config {jsx: 3}];

open PromiseMonad;

// we leave this here for testing
let bootstrap1 = "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx";

module Styles = {
  open Css;

  let voteContainer =
    style([
      maxWidth(`rem(30.)),
      margin2(~v=`px(0), ~h=`auto),
      textAlign(`left),
      selector("label", [
        fontSize(`rem(1.))
      ]),
      selector("aside", [
        fontSize(`rem(0.6))
      ]),
      selector("input, textarea", [
        minWidth(`rem(15.625))
      ]),
      selector("> div", [
        display(`flex),
        flexDirection(`column),
        margin2(~v=`rem(1.), ~h=`px(0))
      ])
    ]);
};

[@react.component]
let make = (~addr, ~host, ~signer, ~options: array(VoteTypes.Option.t)=[||]) => {
  let (voter, setVoter) = React.useState(() => bootstrap1);
  let (voteInBrowser, toggleVoteInBrowser) = React.useState(() => false);
  let (error, set_error) = React.useState(() => None);
  let (choice, setChoice) = React.useState(() => 0);

  Taquito.tezos->Taquito.setProvider({rpc: host, signer});

  let doVote = vote => {
    setChoice(_ => vote);

    // If vote in browser, open tezbridge
    if (voteInBrowser) {
      (
      Taquito.tezos->Taquito.Transfer.call(
        ~entrypoint="vote",
        ~value=Michelson.Int(BigNumber.of_int(vote)),
        ~source=voter,
        ~to_=addr,
        ~amount=0,
        ~fee=100000000,
        ~gasLimit=300000,
        (),
      )
      >>/ (
        err => {
          let errorStr = Error.parse(err)->Error.to_string
          Js.log(errorStr);
          set_error(_ => Some(errorStr));
          %bs.raw
          {|3|};
        }
      )
    )
    ->ignore;
    }
  };

  <div>
    <Message message=?{error} title={""} onClose={_ => set_error(_ => None)} />
    <h3> "Vote for this proposal"->ReasonReact.string </h3>

    <div className={Styles.voteContainer}>
      <div>
        <label>{"Vote Method" -> ReasonReact.string}</label>
        <Switch 
          value={voteInBrowser} 
          onChange={(val_) => toggleVoteInBrowser(_ => val_)}
          leftLabel="Client"
          rightLabel="Browser"
          isOnOff={false}
        />  
      </div>
      <VoterAddressField voter onChange={setVoter} />
      <OptionsField options doVote choice />
      <CLIOutput addr voter choice show={!voteInBrowser}/>
    </div>
  </div>
};