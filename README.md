# Smart contract example with ligo

## Requirements
You will need the following languages, packages, and tools to be installed before you can use this project.

    [LIGO](https://ligolang.org/docs/next/intro/installation/)
    [Docker](https://docs.docker.com/install/)
    [Node >= 10](https://nodejs.dev/how-to-install-nodejs)
    [Opam](https://opam.ocaml.org/doc/Install.html)
    [Python3](https://www.python.org/downloads/)
    [Pytest](https://docs.pytest.org/en/latest/getting-started.html)

## Sandbox node

To run this example you will first need to run a tezos sandbox

    $ alias teztool='docker run -it -v $PWD:/mnt/pwd \
      -e MODE=dind -e DIND_PWD=$PWD \
      -v /var/run/docker.sock:/var/run/docker.sock \
      registry.gitlab.com/nomadic-labs/teztool:latest'

    $ teztool babylonnet sandbox start 18731 --time-between-blocks 7

This command with start a docker container running a node and a backer.
To keep and eye on the logs use `teztool logs sandbox`

The we will use a simple docker image containing ligo. The script pulling and running
this image is in `scripts/ligo`.

## Tezos Client

To interact with the node we install a client using snap .

Download the snap package and install it.

    wget wget https://gitlab.com/abate/tezos-snapcraft/-/raw/master/snaps/tezos_5.1.0_multi.snap?inline=false
    sudo snap install tezos_5.1.0_multi.snap --dangerous
    export PATH=/snap/bin:$PATH
    tezos-mainnet.client -addr localhost -P 18731 config init

The snap package is not signed, therefore we need to specify the `--dangerous` option.

## Init the wallet and compile and originate the contract

We provide a simple makefile.

    make init

adds the bootstrap account secret keys (used in the sandbox node) in the local wallet

    make compile

compile the ligo contract ( in `src/contracts/mono_vote.ligo` )

    make originate
    scripts/gen_real_voters.sh
    python3 ./scripts/gen_voters.py --client "tezos-client" --jsonfile "voters0.json" --addr "mono" --duration 864000

originates the contract

## Test:

    pip3 install "git+https://gitlab.com/nomadic-labs/tezos.git@abate-python-test-package#subdirectory=vendors/tezos-launchers"

## Web UI: 
    To interact with votes through the UI, see `app/README.md`
