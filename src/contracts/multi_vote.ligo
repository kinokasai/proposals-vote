type hash is string;
type ballot is list(string);

type voters is map(address,bool);

type storage is record
  proposals: set(hash);
  voters: voters;
  ballots: list(ballot);
  admin_addr: address;
  timeout: timestamp;
end

type init_storage is record
  proposals: set(hash);
  voters: set(address);
  timeout: timestamp;
end

type action is
| Vote of ballot
| Init of init_storage
| RelinquishControl of address

function relinquish_control(const storage: storage; const addr: address) : storage is
  block { if sender = storage.admin_addr then
          storage.admin_addr := addr
        else
          failwith("what are you trying to relinquish? You're not the admin")
} with storage;

function add_to_map(const result: voters; const key: address): voters is
  block { result[key] := False } with result;

function init_storage(const init_storage: init_storage; const storage: storage) : storage is
  block {
    if sender = storage.admin_addr then begin
      const empty_map : voters = map end;
      storage := record
        proposals = init_storage.proposals;
        voters = set_fold(init_storage.voters, empty_map, add_to_map);
        ballots = ( list end : list(ballot) );
        admin_addr = storage.admin_addr;
        timeout = init_storage.timeout;
      end
    end else
      failwith("Can't init if you're not the admin")
  } with storage;

// check if the ballot contains only valid proposals
function validate_ballot( const ballot : ballot; const proposals : set(hash)) : bool is
  function check (const result : bool ; const hash : hash) : bool is
    block { skip } with (result and set_mem(hash,proposals))
  block { skip } with list_fold(ballot, True, check);

// Record a ballot. The ballot contain the descending order of preferences.
function do_vote(const s: storage; const ballot: ballot; const sender: address) : storage is
  block {
    if validate_ballot(ballot, s.proposals) then begin
      // we record the vote and the fact that the sender cast his preference
      s.ballots := cons(ballot,s.ballots);
      s.voters[sender] := True;
    end else
      failwith("The ballot is not valid")
  } with s;

function main (const action : action ; const s : storage) : (list(operation) * storage) is
  begin
    if now > s.timeout then begin
      case action of
      | Vote(string) -> failwith("vote ended")
      | RelinquishControl(addr) -> s := relinquish_control(s, addr)
      | Init(initial_storage) -> s := init_storage(initial_storage, s)
      end
    end else begin
      case s.voters[sender] of
      | None -> failwith("not voters to vote")
      | Some(has_voted) -> begin
          case has_voted of
          | True -> failwith("voters to vote but already voted")
          | False ->
            case action of
            | Init(storage) -> failwith("The vote is not over. Cannot Init!")
            | RelinquishControl(addr) -> s := relinquish_control(s, addr)
            | Vote(vote) -> s := do_vote(s,vote,sender)
            end
          end
        end
      end
    end
  end with ((nil : list(operation)), s)
